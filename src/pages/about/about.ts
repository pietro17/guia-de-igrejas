import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';

declare var google: any;

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})

export class AboutPage {

  map: any;
  markers:any;

  Igrejas = [
    {
      nome: 'Catedral Metropolitana de Belém',
      latitude: -1.4559083,
      longitude: -48.5047872
    },
    {
      nome: 'Basílica Santuário Nossa Senhora de Nazaré',
      latitude: -1.4526944,
      longitude: -48.4810876
    },
    {
      nome: 'Igreja de Nossa Senhora das Mercês',
      latitude: -1.45087,
      longitude: -48.5009543
    },

    {
      nome: 'Capela do Pão de Santo Antônio',
      latitude: -1.462549,
      longitude: -48.4691396
    },
    {
      nome: 'Igreja de Nossa Senhora do Carmo',
      latitude: -1.4585427,
      longitude: -48.5057971
    }


  ];

  constructor(public navCtrl: NavController,public geolocation: Geolocation, public platform:Platform) {}

  ionViewWillEnter(){
    this.platform.ready().then(() => {
      this.initPage();
    });
  }

  initPage() {
    this.geolocation.getCurrentPosition().then(result => {
      this.loadMap(result.coords.latitude, result.coords.longitude);
    });
  }
  private loadMap(lat, lng) {
    let latLng = new google.maps.LatLng(lat, lng);

    let mapOptions = {
      center: latLng,
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true,
    };

    let element = document.getElementById('map');

    this.map = new google.maps.Map(element, mapOptions);

    let marker = new google.maps.Marker({
      position: latLng,
      title: 'Minha Localização',
      icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
    })

    let content = `
    <div id="myid"  class="item item-thumbnail-left item-text-wrap">
      <ion-item>
        <ion-row>
          <h6>`+marker.title+`</h6>
        </ion-row>
      </ion-item>
    </div>
    `

    this.addInfoWindow(marker, content);
  marker.setMap(this.map);

  this.loadPoints();
}


loadPoints() {
  this.markers = [];

  for (const key of Object.keys(this.Igrejas)) {
    console.log(this.Igrejas[key].nome )
    let latLng = new google.maps.LatLng(this.Igrejas[key].latitude, this.Igrejas[key].longitude);
    let marker = new google.maps.Marker({
          position: latLng,
          title: this.Igrejas[key].nome
        })


        let content = `
        <div id="myid"  class="item item-thumbnail-left item-text-wrap">
          <ion-item>
            <ion-row>
              <h6>`+this.Igrejas[key].nome+`</h6>
            </ion-row>
          </ion-item>
        </div>
        `
        ;

        this.addInfoWindow(marker, content);
        marker.setMap(this.map);
      }

      return this.markers;
    }

addInfoWindow(marker, content) {
  let infoWindow = new google.maps.InfoWindow({
    content: content
  });

  google.maps.event.addListener(marker, 'click', () => {
    infoWindow.open(this.map, marker);

    google.maps.event.addListenerOnce(infoWindow, 'domready', () => {
      document.getElementById('myid').addEventListener('click', () => {
        this.goToEmpresa(marker)
      });
    });
  })
}

goToEmpresa(empresa) {
  alert('Click');
}
}
