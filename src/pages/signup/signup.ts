import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { UserSevice } from './../../providers/user/user.service';




@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {


  signupForm: FormGroup;
  formGroup: SignupPage;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public UserSevice: UserSevice
   ) {



      let emailRegex = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}/i;

  this.signupForm = this.formBuilder.group({
    name: ['', [Validators.required, Validators.minLength(3)]],
    username: ['', [Validators.required, Validators.minLength(3)]],
    email: ['', Validators.compose([Validators.required,Validators.pattern(emailRegex)])],
    password: ['', [Validators.required, Validators.minLength(6)]],

    });


    }


  onSubmit(): void {

    this.UserSevice.create( this.signupForm.value)
    .then(() => {

      console.log('Usuario cadastrado!');
  })};





}
