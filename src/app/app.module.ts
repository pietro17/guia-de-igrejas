import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';


import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { SignupPage } from './../pages/signup/signup';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation'
import {  FirebaseAppConfig, AngularFireModule } from 'angularfire2';
import { UserSevice } from '../providers/user/user.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
//import { AngularFireDatabase } from 'angularfire2/database'



const firebaseAppConfig: FirebaseAppConfig = {

    apiKey: "AIzaSyBptuWX8wKvglQcMRedA-KMVzE2Em_u7HA",
    authDomain: "guia-218623.firebaseapp.com",
    databaseURL: "https://guia-218623.firebaseio.com",
    projectId: "guia-218623",
    storageBucket: "guia-218623.appspot.com",
    messagingSenderId: "500845208930"

};


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    SignupPage,
    TabsPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAppConfig),


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    SignupPage,
    TabsPage
  ],

  providers: [
    StatusBar,
    SplashScreen,
    UserSevice,
    GoogleMaps,
    Geolocation,
    HttpClientModule,
    HttpModule,
    //AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]


})
export class AppModule {}
